/* 
 * Author: Gijs Rijnders
 * 
 * This file is part of nfdumpeditor.
 *
 * nfdumpeditor is free software : you can redistribute it and / or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * nfdumpeditor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with nfdumpeditor.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>
#include <vector>
#include <cstring>
#include <signal.h>

#include "nffile.h"
#include "nfdumpeditor.h"
#include "TimeObject.h"

// LZO compression includes to decompress/compress nfdump files.
#ifdef _WIN32
    // This is the LZO library inclusion statement on Windows operating systems.
    #include "lzo/include/lzoconf.h"
    #include "lzo/include/lzo1x.h"
	#pragma comment(lib, "lzo/lib/liblzo.lib")
#else
    // ... and on *nix based systems
    #include <lzo/lzoconf.h>
    #include <lzo/lzo1x.h>
#endif

// ========================================================================================================================

// Print the help for the manipulation program.
void PrintHelp()
{
	std::cout << "Usage: nfdumpeditor [-f,--input-file] <input_file> [-d,--date-change] <date_change> [-w,--output-file] <output_file> [-v, --verbose]\n" << std::endl;
	std::cout << "-v (--verbose)\tEnable verbose output." << std::endl;
	std::cout << "-f (--input-file)\tEnter the path to the input file." << std::endl;
	std::cout << "-d (--date-change)\tEnter an integer value as a number of years. A negative subtracts years." << std::endl;
	std::cout << "-w (--output-file)\tEnter the path to the output file." << std::endl;
}

// Split the command line arguments into a nice list to parse later.
void SplitCommandLine(const unsigned int count, char** cmdLine, std::vector<std::string>& outCommands)
{
	for (unsigned int i = 0; i < count; ++i)
	{
		outCommands.push_back(cmdLine[i]);
	}
}

// Opens the output file.
const bool OpenOutputFile(const char* fileName)
{
	// Open the data file.
	AppInfo.outFile = fopen(fileName, "wb");
	if (!AppInfo.outFile)
	{
		return false;
	}

	return true;
}

// Writes data to the output file.
const bool WriteDataToOutputFile(const void* const data, const unsigned int dataSize)
{
	// Try to write to the output file.
	const bool success = !!fwrite(data, dataSize, 1, AppInfo.outFile);

	// Write the input data to the output file.
	if (!success && AppInfo.Verbose)
	{
		std::cout << "Failed to write " << dataSize << " bytes to the output file!" << std::endl;
	}

	return success;
}

// Opens the input nfdump file and checks its validity.
const bool OpenNfFile(const char* fileName, file_header_t* const outFileHeader, stat_record_t* const outStatRecord, bool* const compressedData)
{
	// Open the data file.
	AppInfo.hFile = fopen(fileName, "rb");
	if (!AppInfo.hFile)
	{
		return false;
	}

	// Read the file headers into an initial buffer.
	const unsigned int initialBufSize = sizeof(file_header_t) + sizeof(stat_record_t);
	char* const localBuffer = new char[initialBufSize];

	if (!fread(localBuffer, initialBufSize, 1, AppInfo.hFile))
	{
		AppInfo.Release();
		return false;
	}

	// Parse the file and check if the input file is valid.
	file_header_t* fileHeader = reinterpret_cast<file_header_t*>(localBuffer);
	if (fileHeader->magic != MAGIC || fileHeader->version != LAYOUT_VERSION_1)
	{
		AppInfo.Release();
		return false;
	}

	// Prepare the processed input headers for the next step of reading.
	memcpy(outFileHeader, fileHeader, sizeof(file_header_t));
	memcpy(outStatRecord, (char*)fileHeader + sizeof(file_header_t), sizeof(stat_record_t));
	*compressedData = !!(fileHeader->flags & FLAG_COMPRESSED);

	// Release initial headers buffer.
	delete[] localBuffer;

	return true;
}

// Traverses all blocks in the input file, including taking care of compression.
const bool TraverseFileBlocks(const file_header_t* const fileHeader, const bool compressedData)
{
	bool returnValue = true;

	// Loop the blocks inside the input file.
	for (unsigned int i = 0; i < fileHeader->NumBlocks; ++i)
	{
		// Read the next block into a local buffer.
		data_block_header_t blockHeader;
		if (!fread(&blockHeader, sizeof(blockHeader), 1, AppInfo.hFile))
		{
			returnValue = false;
			break;
		}

		// Save the initial size of the current block.
		const unsigned int blockSize = blockHeader.size;

		// Now we know the size of the block, let's read the entire block into a buffer.
		char* blockBuffer = new char[blockSize];
		if (!fread(blockBuffer, blockSize, 1, AppInfo.hFile))
		{
			// If the user wants verbose output, present an immediate error message.
			if (AppInfo.Verbose)
			{
				std::cout << "Failed to read block #" << i << " from the input file!" << std::endl;
			}

			delete[] blockBuffer;
			returnValue = false;
			break;
		}

		// Only parse block type 2 blocks.
		if (blockHeader.id == DATA_BLOCK_TYPE_1 || blockHeader.id == DATA_BLOCK_TYPE_2)
		{
			// If the records in the opened file are compressed, we first need to decompress them.
			if (compressedData)
			{
				unsigned char* lzoBuffer = new unsigned char[blockSize * 8];
				lzo_uint new_len;
				if (lzo1x_decompress((unsigned char*)blockBuffer, blockSize, lzoBuffer, &new_len, NULL) != LZO_E_OK)
				{
					// If the user wants verbose output, present an immediate error message.
					if (AppInfo.Verbose)
					{
						std::cout << "Failed to decompress block #" << i << "!" << std::endl;
					}

					// Decompression has failed! Clean up the used resources.
					delete[] lzoBuffer;
					delete[] blockBuffer;
					returnValue = false;
					break;
				}

				// Decompression succeeded, delete the original data buffer and continue working with the decompressed one.
				delete[] blockBuffer;
				blockBuffer = (char*)lzoBuffer;
				blockHeader.size = new_len;
			}

			// Get a pointer to the first record in the block.
			record_header_t* record = reinterpret_cast<record_header_t*>(blockBuffer);

			// Loop through the records in the current block.
			for (unsigned int r = 0; r < blockHeader.NumRecords; ++r)
			{
				// If we encounter common records for the old nfdump format or the new one, we may need to change something here.
				if (record->type == CommonRecordType)
				{
					common_record_t* const common_record = reinterpret_cast<common_record_t*>(record);

					// Let's change the timestamp for this record.
					common_record->first = (uint32_t)TimeObject(common_record->first).AddYears(AppInfo.DateChange);
					common_record->last = (uint32_t)TimeObject(common_record->last).AddYears(AppInfo.DateChange);
				}
				else if (record->type == CommonRecordV0Type)
				{
					common_record_v0_t* const common_record = reinterpret_cast<common_record_v0_t*>(record);

					// Let's change the timestamp for this record.
					common_record->first = (uint32_t)TimeObject(common_record->first).AddYears(AppInfo.DateChange);
					common_record->last = (uint32_t)TimeObject(common_record->last).AddYears(AppInfo.DateChange);
				}
				else
				{
					// This record is not of any use to us, let's skip it but notify the user that we skipped it.
					++AppInfo.SkippedRecords;
				}

				// Add the size of the record to the pointer to follow up to the next record.
				record = reinterpret_cast<record_header_t*>((char*)record + record->size);
			}

			// We need to recompress possibly compressed blocks and write them back to the file including any modifications.
			if (compressedData)
			{
				// Allocate some working memory for the compression routine.
				lzo_align_t __LZO_MMODEL wrkmem[(LZO1X_1_MEM_COMPRESS + (sizeof(lzo_align_t) - 1)) / sizeof(lzo_align_t)];

				unsigned char* lzoBuffer = new unsigned char[blockHeader.size];
				lzo_uint new_len;
				if (lzo1x_1_compress((unsigned char*)blockBuffer, blockHeader.size, lzoBuffer, &new_len, wrkmem) != LZO_E_OK)
				{
					// If the user wants verbose output, present an immediate error message.
					if (AppInfo.Verbose)
					{
						std::cout << "Failed to compress block #" << i << "!" << std::endl;
					}

					// Compression has failed! Clean up the used resources.
					delete[] lzoBuffer;
					delete[] blockBuffer;
					returnValue = false;
					break;
				}

				// Decompression succeeded, delete the original data buffer and continue working with the decompressed one.
				delete[] blockBuffer;
				blockBuffer = (char*)lzoBuffer;
				blockHeader.size = new_len;
			}
		}

		// Now we write the processed (and possibly recompressed) block to the output file, including the manipulated block header.
		WriteDataToOutputFile(&blockHeader, sizeof(blockHeader));
		WriteDataToOutputFile(blockBuffer, blockHeader.size);

		// Release the block buffer before moving to the next block.
		delete[] blockBuffer;
	}

	return returnValue;
}

// Called when a Ctrl+C signal is entered by the user.
void signal_callback_handler(int signum)
{
	// Catch the interrupt and notify the user about it.
	std::cout << "Interrupt was caught, shutting down..." << std::endl;

	// Release used resources.
	AppInfo.Release();

	// Terminate the program.
	exit(signum);
}

int main(int argc, char** argv)
{
	std::vector<std::string> commands;
	SplitCommandLine(argc, argv, commands);

	bool isHelp = false;
	std::string inputFile;
	std::string outputFile;

	// Register the Ctrl+C callback handler.
	signal(SIGINT, signal_callback_handler);

	// Show a welcome message.
	std::cout << "NfDump file format date changer: Gijs Rijnders" << std::endl << std::endl;

	// Check command line arguments.
	const unsigned int aCount = commands.size();
	for (unsigned int i = 0; i < aCount; ++i)
	{
		if (commands[i].compare("?") == 0 || commands[i].compare("-h") == 0 || commands[i].compare("--help") == 0)
		{
			isHelp = true;
		}
		else if (commands[i].compare("-v") == 0 || commands[i].compare("--verbose") == 0)
		{
			AppInfo.Verbose = true;
		}
		else if ((commands[i].compare("-f") == 0 || commands[i].compare("--input-file") == 0) && i + 1 <= aCount)
		{
			inputFile = commands[i + 1];
		}
		else if ((commands[i].compare("-w") == 0 || commands[i].compare("--output-file") == 0) && i + 1 <= aCount)
		{
			outputFile = commands[i + 1];
		}
		else if ((commands[i].compare("-d") == 0 || commands[i].compare("--date-change") == 0) && i + 1 <= aCount)
		{
			std::stringstream dateStream;
			dateStream << commands[i + 1];
			dateStream >> AppInfo.DateChange;
		}
	}

	// Display help if called for or if invalid input was detected.
	if (isHelp || inputFile.empty() || outputFile.empty())
	{
		// Print the help to clarify the program usage and quit.
		PrintHelp();
		return 0;
	}

	// Check whether the input file and output file are different. We cannot read and write to the same file.
	if (inputFile.compare(outputFile) == 0)
	{
		std::cout << "The input file and output file cannot be equal!" << std::endl;
		return 0;
	}

	// Open input file and parse it.
	file_header_t fileHeader;
	stat_record_t statRecord;
	bool hasCompressedRows;

	// First try to open the output file. If this doesn't work, we don't need to open the input file either.
	if (OpenOutputFile(outputFile.c_str()))
	{
		// Successfully opened the output file.
		std::cout << "Succesfully opened output file for writing at " << outputFile << std::endl;

		// Now try to open the input file. If this works, we can start parsing the input data.
		if (OpenNfFile(inputFile.c_str(), &fileHeader, &statRecord, &hasCompressedRows))
		{
			// Let's show the user some stats.
			std::cout << "Succesfully opened " << inputFile << ", containing " << fileHeader.NumBlocks << " blocks and " << statRecord.numflows << " flows." << std::endl;
			std::cout << "This file " << (hasCompressedRows ? "contains " : "does not contain ") << "compressed blocks" << std::endl;

			// Let the user know that we are modifying global file contents.
			if (AppInfo.Verbose)
			{
				std::cout << "Changing first- and last seen values of the file globally..." << std::endl;
			}

			// First change the first and last fields in the stat_record header.
			statRecord.first_seen = (uint32_t)TimeObject(statRecord.first_seen).AddYears(AppInfo.DateChange);
			statRecord.last_seen = (uint32_t)TimeObject(statRecord.last_seen).AddYears(AppInfo.DateChange);

			// Write the initial headers to the output file. They should not change throughout the processing of blocks.
			if (WriteDataToOutputFile(&fileHeader, sizeof(fileHeader)) && WriteDataToOutputFile(&statRecord, sizeof(statRecord)))
			{
				// Initial writing to the output file has succeeded, let's traverse the input file blocks.
				if (TraverseFileBlocks(&fileHeader, hasCompressedRows))
				{
					std::cout << "Processed " << statRecord.numflows - AppInfo.SkippedRecords << ", skipped " << AppInfo.SkippedRecords << std::endl;
				}
			}
			else
			{
				std::cout << "Failed to write the modified file headers to the output file!" << std::endl;
			}
		}
		else
		{
			std::cout << "Failed to open the input file " << inputFile << "!" << std::endl;
		}
	}
	else
	{
		std::cout << "Failed to open the output file at " << outputFile << "!" << std::endl;
	}

	// Close file handles and release buffers.
	AppInfo.Release();

	return 0;
}
