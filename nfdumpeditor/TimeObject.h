/*
 * Author: Gijs Rijnders
 *
 * This file is part of nfdumpeditor.
 *
 * nfdumpeditor is free software : you can redistribute it and / or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * nfdumpeditor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with nfdumpeditor.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TIMEOBJECT_H
#define _TIMEOBJECT_H

#include <time.h>
#include <string>

// Represents a datetime object used in Netflow records.
class TimeObject
{
private:
	time_t rawTime;
public:
	TimeObject(const time_t rawTime);
	~TimeObject();

	// Getters.
	const time_t GetRawTime() const;
	const std::string GetDateTimeString() const;
	void GetDateTimeStruct(struct tm* const timeStruct) const;

	// Setters.
	TimeObject& AddYears(const int years);

	// Type casting operator to implicitly allow casting to time_t.
	operator time_t() const { return this->rawTime; }
};

#endif