/*
 * Author: Gijs Rijnders
 *
 * This file is part of nfdumpeditor.
 *
 * nfdumpeditor is free software : you can redistribute it and / or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * nfdumpeditor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with nfdumpeditor.
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _NFDUMPEDITOR_H
#define _NFDUMPEDITOR_H

#include <stdint.h>
#include <iostream>

// ========================================================================================================================

// Global information structure that holds file handles and buffer handles.
struct AppInfo_t
{
	FILE* hFile;
	FILE* outFile;
	uint64_t OutputFileSize;
	uint64_t SkippedRecords;
	int DateChange;
	bool Verbose;

	AppInfo_t()
	{
		this->hFile = NULL;
		this->SkippedRecords = 0;
		this->OutputFileSize = 0;
		this->DateChange = 0;
	};

	void Release()
	{
		if (this->hFile)
		{
			fclose(this->hFile);
		}

		if (this->outFile)
		{
			fclose(this->outFile);
		}
	};
} AppInfo;

#endif
